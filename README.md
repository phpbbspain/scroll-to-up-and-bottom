# Scroll to Up and Bottom
A phpBB 3.1 extension that adds a Scroll to Up and Bottom button making it easy to return to the up and bottom of a page.

## Extensión validada oficialmente por phpBB 
[Official Topic - Scroll To Up and Bottom](https://www.phpbb.com/community/viewtopic.php?f=536&t=2355801)

[Official Support - Scroll To Up and Bottom](https://www.phpbb.com/customise/db/extension/scroll_to_up_and_bottom/support)

## Corrección de errores
Para cualquier cambio a realizar, simplemente editar para realizar el cambio y "Pull Request".

## License
[GNU General Public License v2](http://opensource.org/licenses/GPL-2.0)

## Autor
ThE KuKa (Raúl Arroyo Monzo)

## © 2003 / 2016 [phpBB España](http://www.phpbb-es.com)

![phpBB Spain](http://www.phpbb-es.com/images/logo_es.png)